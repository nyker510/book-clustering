# coding: utf-8
"""
utility functions
"""
from logging import getLogger, StreamHandler, FileHandler, Formatter
from time import sleep

from selenium import webdriver
from urllib.parse import urlencode

from .settings import request_interval

__author__ = "nyk510"

driver = webdriver.PhantomJS()


def get_logger(name, log_level="DEBUG", output_file=None, handler_level="INFO"):
    """
    :param str name:
    :param str log_level:
    :param str | None output_file:
    :param str handler_level: handler がログを送出する level
    :return: logger
    """
    logger = getLogger(name)

    formatter = Formatter("[%(levelname)s %(name)s] %(asctime)s: %(message)s")

    handler = StreamHandler()
    logger.setLevel(log_level)
    handler.setLevel(handler_level)

    handler.setFormatter(formatter)
    logger.addHandler(handler)

    if output_file:
        file_handler = FileHandler(output_file)
        file_handler.setFormatter(formatter)
        file_handler.setLevel(handler_level)
        logger.addHandler(file_handler)

    return logger


def fetch_with_js(url, params=None, interval=None):
    """
    js を考慮して web ページの内容を取得する

    :param str url:
    :param dict params:
    :param int interval:
    :return:
    """
    if interval is None:
        interval = request_interval
    if params:
        url += "?" + urlencode(params)
    driver.get(url)
    sleep(interval)
    data = driver.page_source.encode("utf-8")
    return data
