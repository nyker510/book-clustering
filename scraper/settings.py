# coding: utf-8
"""
"""

import os

OUTPUT_DIR = os.getenv("OUTPUT_DIR", "./data/raw")
request_interval = 1

if os.path.exists(OUTPUT_DIR) is False:
    os.makedirs(OUTPUT_DIR)